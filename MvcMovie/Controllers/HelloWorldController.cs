﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMovie.Controllers
{
    public class HelloWorldController : Controller
    {
        // GET: HelloWorld
        public ActionResult Index()
        {
            return View();
        }
        //public string Welcome()
        //{
        //    return "This is <i>welcome</i> action result";
        //}
        public ActionResult Welcome(string name, int num = 1)
        {
            ViewBag.Message = "Hello " + name;
            ViewBag.NumTimes = num;
            return View();
        }
    }
}